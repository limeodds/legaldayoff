package com.example.legaldayoff;

import com.example.legaldayoff.repository.db.LegalDayOffDao;
import com.example.legaldayoff.repository.db.entity.LegalDayOffEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LegalDayOffApplicationTests {

  @Autowired
  LegalDayOffDao legalDayOffDao;

  @Test
  public void contextLoads() {
    List<LegalDayOffEntity> allDaysOff = legalDayOffDao.findInInterval(LocalDate.now(), LocalDate.now());
  }

}
