package com.example.legaldayoff.repository.converter;

import com.example.legaldayoff.domain.LegalDayOff;
import com.example.legaldayoff.repository.db.entity.LegalDayOffEntity;
import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;

import static org.assertj.core.api.Assertions.assertThat;

public class LegalDayOffConverterTest {
  private static final Long id = 123L;
  private static final LocalDate dayOff = LocalDate.of(2018, Month.MAY, 1);

  @Test
  public void toDomain() {
    //given
    LegalDayOffEntity entity = LegalDayOffEntity.builder()
                                                .id(id)
                                                .dayOff(dayOff)
                                                .build();
    //when
    LegalDayOff domainObj = LegalDayOffConverter.toDomain(entity);
    //then
    assertThat(domainObj.getId()).isEqualTo(id);
    assertThat(domainObj.getDayOff()).isEqualTo(dayOff);
  }

  @Test
  public void toDb() {
    //given
    LegalDayOff domainObj = LegalDayOff.builder()
                                                .id(id)
                                                .dayOff(dayOff)
                                                .build();
    //when
    LegalDayOffEntity dbObj = LegalDayOffConverter.toDb(domainObj);
    //then
    assertThat(dbObj.getId()).isEqualTo(id);
    assertThat(dbObj.getDayOff()).isEqualTo(dayOff);
  }
}