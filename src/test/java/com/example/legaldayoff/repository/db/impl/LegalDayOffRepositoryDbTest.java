package com.example.legaldayoff.repository.db.impl;

import com.example.legaldayoff.domain.LegalDayOff;
import com.example.legaldayoff.repository.converter.LegalDayOffConverter;
import com.example.legaldayoff.repository.db.LegalDayOffDao;
import com.example.legaldayoff.repository.db.entity.LegalDayOffEntity;
import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * See https://www.timeanddate.com/calendar/?year=1930&country=1
 */
public class LegalDayOffRepositoryDbTest {
  private final List<LegalDayOffEntity> daysOff = Arrays.asList(new LegalDayOffEntity(1L, LocalDate.of(1930, Month.JANUARY, 1)),
                                                                new LegalDayOffEntity(2L, LocalDate.of(1930, Month.JANUARY, 2)),
                                                                new LegalDayOffEntity(3L, LocalDate.of(1930, Month.JANUARY, 23)),
                                                                new LegalDayOffEntity(4L, LocalDate.of(1930, Month.APRIL, 9)),
                                                                new LegalDayOffEntity(5L, LocalDate.of(1930, Month.MAY, 1)),
                                                                new LegalDayOffEntity(6L, LocalDate.of(1930, Month.MAY, 27)));

  @Test
  public void findInInterval() {
    //given
    LocalDate startInclusive = LocalDate.of(1930, Month.JANUARY, 1);
    LocalDate endInclusive = LocalDate.of(1930, Month.MAY, 27);
    LegalDayOffDao legalDayOffDao = mock(LegalDayOffDao.class);

    LegalDayOffRepositoryDb legalDayOffRepository = new LegalDayOffRepositoryDb();
    legalDayOffRepository.setLegalDayOffDao(legalDayOffDao);

    //when
    when(legalDayOffDao.findInInterval(startInclusive, endInclusive)).thenReturn(daysOff);
    List<LegalDayOff> dayOffsDomain = daysOff.stream().map(LegalDayOffConverter::toDomain).collect(toList());

    //then

    assertThat(legalDayOffRepository.findInInterval(startInclusive, endInclusive)).hasSize(6);
    assertThat(legalDayOffRepository.findInInterval(startInclusive, endInclusive)).isEqualTo(dayOffsDomain);
  }
}