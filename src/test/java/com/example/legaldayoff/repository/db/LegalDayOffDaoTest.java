package com.example.legaldayoff.repository.db;

import com.example.legaldayoff.repository.db.entity.LegalDayOffEntity;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * See https://www.timeanddate.com/calendar/?year=1931&country=1
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class LegalDayOffDaoTest {
  private final List<LegalDayOffEntity> daysOff = Arrays.asList(new LegalDayOffEntity(null, LocalDate.of(1930, Month.JANUARY, 1)),
                                                                new LegalDayOffEntity(null, LocalDate.of(1930, Month.JANUARY, 2)),
                                                                new LegalDayOffEntity(null, LocalDate.of(1930, Month.JANUARY, 23)),
                                                                new LegalDayOffEntity(null, LocalDate.of(1930, Month.APRIL, 9)),
                                                                new LegalDayOffEntity(null, LocalDate.of(1930, Month.MAY, 1)),
                                                                new LegalDayOffEntity(null, LocalDate.of(1930, Month.MAY, 27)));

  @Autowired
  private LegalDayOffDao legalDayOffDao;

  @Before
  public void setup() {
    daysOff.forEach(ldo -> {
      LegalDayOffEntity saved = legalDayOffDao.add(ldo);
      ldo.setId(saved.getId());
    });
  }

  @Test
  public void findInInterval() {
    assertThat(legalDayOffDao.findInInterval(LocalDate.of(1930, Month.JANUARY, 1), LocalDate.of(1930, Month.APRIL, 8))).hasSize(3);
    assertThat(legalDayOffDao.findInInterval(LocalDate.of(1930, Month.JANUARY, 2), LocalDate.of(1930, Month.APRIL, 9))).hasSize(3);
    assertThat(legalDayOffDao.findInInterval(LocalDate.of(1930, Month.JANUARY, 3), LocalDate.of(1930, Month.APRIL, 10))).hasSize(2);
    assertThat(legalDayOffDao.findInInterval(LocalDate.of(1930, Month.FEBRUARY, 3), LocalDate.of(1930, Month.APRIL, 7))).hasSize(0);

  }

  @After
  public void cleanup() {
    daysOff.forEach(ldo -> legalDayOffDao.deleteById(ldo.getId()));
  }
}