package com.example.legaldayoff.service;

import com.example.legaldayoff.domain.LegalDayOff;
import com.example.legaldayoff.repository.LegalDayOffRepository;
import org.junit.Test;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class LegalDayOffServiceTest {
  LocalDate startInclusive = LocalDate.of(1931, Month.JANUARY, 1);
  LocalDate endInclusive = LocalDate.of(1931, Month.JANUARY, 9);
  LegalDayOffRepository legalDayOffRepository = Mockito.mock(LegalDayOffRepository.class);

  private final List<LegalDayOff> daysOff = Arrays.asList(new LegalDayOff(null, LocalDate.of(1931, Month.JANUARY, 1)),
                                                          new LegalDayOff(null, LocalDate.of(1931, Month.JANUARY, 2)));

  @Test
  public void findInInterval() {
    //given
    LegalDayOffService ldos = new LegalDayOffService();
    ldos.setLegalDayOffRepository(legalDayOffRepository);

    //when
    Mockito.when(legalDayOffRepository.findInInterval(startInclusive, endInclusive)).thenReturn(daysOff);

    //then
    assertThat(ldos.findInInterval(startInclusive, endInclusive)).hasSize(2);
  }

  @Test
  public void findWorkingDaysInInterval() {
    //given
    LegalDayOffService ldos = new LegalDayOffService();
    ldos.setLegalDayOffRepository(legalDayOffRepository);

    //when
    Mockito.when(legalDayOffRepository.findInInterval(startInclusive, endInclusive)).thenReturn(daysOff);

    //then
    assertThat(ldos.findInInterval(startInclusive, endInclusive)).hasSize(2);
  }
}