package com.example.legaldayoff;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LegalDayOffApplication {

	public static void main(String[] args) {
		SpringApplication.run(LegalDayOffApplication.class, args);
	}
}
