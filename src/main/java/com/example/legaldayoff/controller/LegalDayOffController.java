package com.example.legaldayoff.controller;

import com.example.legaldayoff.domain.DaysNo;
import com.example.legaldayoff.service.LegalDayOffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

@Component
@Path("/")
public class LegalDayOffController {
  private static final String dateTimePattern = "dd-MM-yyyy";
  private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(dateTimePattern);

  @Autowired
  private LegalDayOffService legalDayOffService;

  @GET
  @Path("/days-no/{start}/{end}")
  @Produces(MediaType.APPLICATION_JSON)
  public DaysNo getDaysNo(@PathParam("start") String startInclusiveStr,
                          @PathParam("end") String endInclusiveStr) {

//    try {
      LocalDate startInclusive = LocalDate.parse(startInclusiveStr, dateTimeFormatter);
      LocalDate endInclusive = LocalDate.parse(endInclusiveStr, dateTimeFormatter);

      return legalDayOffService.getDaysNo(startInclusive, endInclusive);
//    } catch (DateTimeParseException e) {
//      return new ResponseEntity<>("'start' and 'end' must have '" + dateTimePattern + "' format", null, HttpStatus.BAD_REQUEST);
//    }
  }

//  @PostMapping(value = "")
//  public ResponseEntity<?> addDayOff(@RequestBody LegalDayOffJson newLegalDayOff) {
//    LegalDayOff legalDayOff = LegalDayOff.builder()
//                                         .dayOff(LocalDate.parse(newLegalDayOff.getDayOff(), dateTimeFormatter))
//                                         .build();
//
//    Long id = legalDayOffService.add(legalDayOff).getId();
//
//    // Set the location header for the newly created resource
//    HttpHeaders responseHeaders = new HttpHeaders();
//    URI newPollUri = ServletUriComponentsBuilder.fromCurrentRequest()
//                                                .path("/{id}")
//                                                .buildAndExpand(id)
//                                                .toUri();
//    responseHeaders.setLocation(newPollUri);
//    return new ResponseEntity<>(null, responseHeaders, HttpStatus.CREATED);
//  }
}
