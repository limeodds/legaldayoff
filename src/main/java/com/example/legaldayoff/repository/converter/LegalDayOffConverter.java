package com.example.legaldayoff.repository.converter;

import com.example.legaldayoff.domain.LegalDayOff;
import com.example.legaldayoff.repository.db.entity.LegalDayOffEntity;

public class LegalDayOffConverter {
  public static LegalDayOff toDomain(LegalDayOffEntity dbObj) {
    return LegalDayOff.builder()
                      .id(dbObj.getId())
                      .dayOff(dbObj.getDayOff())
                      .build();
  }

  public static LegalDayOffEntity toDb(LegalDayOff domainObj) {
    return LegalDayOffEntity.builder()
                            .id(domainObj.getId())
                            .dayOff(domainObj.getDayOff())
                            .build();
  }
}
