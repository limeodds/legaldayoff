package com.example.legaldayoff.repository.db;

import com.example.legaldayoff.repository.db.entity.LegalDayOffEntity;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class LegalDayOffDao extends ABaseDao<LegalDayOffEntity> {

  public List<LegalDayOffEntity> findInInterval(LocalDate startInclusive, LocalDate endInclusive) {
    String query = "from " + LegalDayOffEntity.class.getSimpleName() + " where dayOff >= :startInclusive and dayOff <= :endInclusive";

    Map<String, Object> params = new HashMap<>();
    params.put("startInclusive", startInclusive);
    params.put("endInclusive", endInclusive);

    return findByNamedParameters(query, params);
  }

}
