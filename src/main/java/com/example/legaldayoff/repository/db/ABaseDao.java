package com.example.legaldayoff.repository.db;

import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

@Transactional
public abstract class ABaseDao<T> {
  private final Class<T> clazz;

  @PersistenceContext
  private EntityManager entityManager;

  @SuppressWarnings("unchecked")
  public ABaseDao() {
    this.clazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
  }

  public T findById(Long id) {
    return entityManager.find(clazz, id);
  }

  @SuppressWarnings("unchecked")
  public List<T> findAll() {
    return entityManager.createQuery("from " + clazz.getName()).getResultList();
  }

  public T add(T entity) {
    entityManager.persist(entity);
    entityManager.flush();
    return entity;
  }

  public void update(T entity) {
    entityManager.merge(entity);
  }

  public void delete(T entity) {
    entityManager.remove(entity);
  }

  public void deleteById(Long entityId) {
    T entity = findById(entityId);
    delete(entity);
  }

  public List<T> findByNamedParameters(String query, Map<String, Object> params) {
    TypedQuery<T> typedQuery = entityManager.createQuery(query, clazz);

    params.forEach((key, value) -> typedQuery.setParameter(key, value));

    return typedQuery.getResultList();
  }
}
