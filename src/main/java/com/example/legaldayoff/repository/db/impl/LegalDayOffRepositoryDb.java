package com.example.legaldayoff.repository.db.impl;

import com.example.legaldayoff.domain.LegalDayOff;
import com.example.legaldayoff.repository.LegalDayOffRepository;
import com.example.legaldayoff.repository.converter.LegalDayOffConverter;
import com.example.legaldayoff.repository.db.LegalDayOffDao;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Repository
public class LegalDayOffRepositoryDb implements LegalDayOffRepository {
  @Autowired @Setter
  private LegalDayOffDao legalDayOffDao;

  @Override
  public LegalDayOff add(LegalDayOff legalDayOff) {
    return LegalDayOffConverter.toDomain(legalDayOffDao.add(LegalDayOffConverter.toDb(legalDayOff)));
  }

  @Override
  public void delete(long id) {
    legalDayOffDao.deleteById(id);
  }

  @Override
  public List<LegalDayOff> findInInterval(LocalDate startInclusive, LocalDate endInclusive) {
    return legalDayOffDao.findInInterval(startInclusive, endInclusive)
                         .stream()
                         .map(LegalDayOffConverter::toDomain).collect(toList());
  }
}
