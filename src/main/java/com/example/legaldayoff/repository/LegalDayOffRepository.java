package com.example.legaldayoff.repository;

import com.example.legaldayoff.domain.LegalDayOff;

import java.time.LocalDate;
import java.util.List;

public interface LegalDayOffRepository {
  LegalDayOff add(LegalDayOff legalDayOff);

  void delete(long id);

  List<LegalDayOff> findInInterval(LocalDate startInclusive, LocalDate endInclusive);
}
