package com.example.legaldayoff.domain;

import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@Builder
@EqualsAndHashCode(of = {"id", "dayOff"})
public class LegalDayOff {
  private Long id;
  private LocalDate dayOff;
}
