package com.example.legaldayoff.domain.json;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor @NoArgsConstructor
@Builder
@EqualsAndHashCode()
public class LegalDayOffJson {
  private String dayOff;
}
