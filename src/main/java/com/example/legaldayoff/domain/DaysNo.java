package com.example.legaldayoff.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor @Builder
public class DaysNo {
  private int legalDaysOffNo;
  private int workingDaysNo;
}
