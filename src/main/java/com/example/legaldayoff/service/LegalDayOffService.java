package com.example.legaldayoff.service;

import com.example.legaldayoff.domain.DaysNo;
import com.example.legaldayoff.domain.LegalDayOff;
import com.example.legaldayoff.repository.LegalDayOffRepository;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

@Service
public class LegalDayOffService {
  @Autowired
  @Setter
  private LegalDayOffRepository legalDayOffRepository;

  private final EnumSet<DayOfWeek> weekWorkingDays = EnumSet.of(DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.WEDNESDAY,
                                                                DayOfWeek.THURSDAY, DayOfWeek.FRIDAY);

  public LegalDayOff add(LegalDayOff legalDayOff) {
    return legalDayOffRepository.add(legalDayOff);
  }

  public void delete(long id) {
    legalDayOffRepository.delete(id);
  }

  public List<LegalDayOff> findInInterval(LocalDate startInclusive, LocalDate endInclusive) {
    return legalDayOffRepository.findInInterval(startInclusive, endInclusive);
  }

  public DaysNo getDaysNo(LocalDate startInclusive, LocalDate endInclusive) {
    Set<LocalDate> daysOff = findInInterval(startInclusive, endInclusive).stream()
                                                                         .map(LegalDayOff::getDayOff)
                                                                         .collect(toSet());

    List<LocalDate> workingDays = new ArrayList<>();

    LocalDate idx = startInclusive;
    while (idx.isBefore(endInclusive) || idx.isEqual(endInclusive)) {
      if (!daysOff.contains(idx) && weekWorkingDays.contains(idx.getDayOfWeek())) {
        workingDays.add(idx);
      }
      idx = idx.plusDays(1);
    }

    return DaysNo.builder()
                 .legalDaysOffNo(daysOff.size())
                 .workingDaysNo(workingDays.size())
                 .build();
  }
}
