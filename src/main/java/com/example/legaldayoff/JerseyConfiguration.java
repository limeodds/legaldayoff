package com.example.legaldayoff;

import com.example.legaldayoff.controller.LegalDayOffController;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.ws.rs.ApplicationPath;

@Configuration
@ApplicationPath("/v1/legal-day-off")
public class JerseyConfiguration extends ResourceConfig {

  @PostConstruct
  public void setUp() {
    register(LegalDayOffController.class);
  }
}
