CREATE DATABASE vacation DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
CREATE USER 'test'@'localhost' IDENTIFIED BY 'test123';
GRANT ALL PRIVILEGES ON vacation.* TO 'test'@'localhost';

drop table if exists T_LEGAL_DAY_OFF;
create table T_LEGAL_DAY_OFF (
  ID INT not null AUTO_INCREMENT,
  DAY_OFF DATE not null UNIQUE,
  PRIMARY KEY (id));
